import Classes from './App.css';
import Cockpit from '../components/Cockpit/Cockpit';
import Persons from '../components/Persons/Persons';
import React, { Component } from 'react';
import withClass from '../hoc/withClass';
import Aux from '../hoc/Aux';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [
        { id: "alksd1", name: 'Nayan Biswas', age: 22 },
        { id: "alksd2", name: 'S M HEMEL', age: 23 },
        { id: "alksd3", name: 'Utshwa', age: 24 }
      ],
      showPersons: false,
      toggleClicked: 0
    };
    //console.log('App.js Called constructor');
  }

  componentWillMount() {
    //console.log('App.js Called WillMount');
  }
  componentDidMount() {
    //console.log('App.js Called DidMount');
  }

  togglePersonsHandler = () => {
    const doseShow = this.state.showPersons;
    this.setState((previousState, props) => {
      return {
        showPersons: !doseShow,
        toggleClicked: previousState.toggleClicked + 1
      }
    });
  }
  deletePersonHandler = (index) => {
    const persons = [...this.state.persons];
    persons.splice(index, 1);
    this.setState({ persons: persons });
  }
  nameChangedHandler = (event, id) => {
    //console.log(event);
    //console.log("Midilware");
    //console.log(id);
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })
    const person = {
      ...this.state.persons[personIndex]
    }
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({ persons: persons });
  }
  render() {
    //console.log('In App.js Called render');
    let persons = null;
    if (this.state.showPersons) {
      persons = <Persons
        persons={this.state.persons}
        clicked={this.deletePersonHandler}
        changed={this.nameChangedHandler}
      />
    }
    return (
      <Aux>
        <Cockpit
          showPersons={this.state.showPersons}
          persons={this.state.persons}
          clicked={this.togglePersonsHandler} />
        {persons}
      </Aux>
    );
  }
}

export default withClass(App, Classes.App);
