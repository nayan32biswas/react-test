import Classes from './Person.css';
import React, { Component } from 'react';
import withClass from '../../../hoc/withClass';
import Aux from '../../../hoc/Aux';
import PropTypes from 'prop-types';

class Person extends Component {
    constructor(props) {
        super(props);
        console.log('constructor', props);
    }
    componentWillMount() {
        //console.log('Person Called WillMount');
    }
    componentDidMount() {
        if(this.props.position === 0){
            this.inputElement.focus();
        }
        //console.log('Person Called DidMount');
    }
    render() {
        //console.log('In Person.js Called render\n\n\n');
        return (
            <Aux>
                < p onClick={this.props.click}> My name is {this.props.name} and I am {this.props.age} years old </p>
                < p >{this.props.children}</p>
                <input
                    ref={(inp) => (this.inputElement = inp)}
                    type='text'
                    onChange={this.props.changed} value={this.props.name} />
            </Aux>
        )
    }
}

Person.propTypes = {
    age: PropTypes.number,
    click: PropTypes.func,
    name: PropTypes.string,
    changed: PropTypes.func
}

export default withClass(Person, Classes.Person);
