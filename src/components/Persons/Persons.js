import Person from './Person/Person';
import React, { Component } from 'react';

class Persons extends Component {
        constructor(props){
                super(props);
                console.log('constructor', props);
        }
        componentWillMount() {
                //console.log('componentWillMount'); 
        }
        componentDidMount() {
                //console.log('componentDidMount');
        }
        componentWillReceiveProps(nextProps){
                //console.log("componentWillReceiveProps", nextProps);
        }
        componentWillUpdate(nextProps, nextState){
                //console.log("componentWillUpdate", nextProps, nextState);
        }
        componentDidUpdate(){
                //console.log("ComponentDidUpdate");
        }
        shouldComponentUpdate(nextProps, nextState){
                //console.log("shouldComponentUpadte", nextProps, nextState);
                return nextProps.persons !== this.props.persons;
        }
        render() {
                //console.log('render');
                return this.props.persons.map((person, index) => {
                        return <Person
                                click={() => this.props.clicked(index)}
                                name={person.name}
                                position={index}
                                age={person.age}
                                key={person.id}
                                changed={(event) => this.props.changed(event, person.id)} />
                })
        }
}
export default Persons;