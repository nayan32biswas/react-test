import Aux from '../../hoc/Aux';
import Classes from './Cockpit.css';
import React, { Component } from 'react';


class Cockpit extends Component {
    constructor(props) {
        super(props);
        console.log('constructor', props);
    }
    componentWillMount() {
        //console.log('Cockpit.js Called WillMount');
    }
    componentDidMount() {
        //console.log('Cockpit.js Called DidMount');
    }
    render() {
        //console.log('In Cockpit.js Called render');
        const assignedClasses = [];
        let btnClass = Classes.Button;
        if (this.props.showPersons) {
            btnClass = [Classes.Button, Classes.Red].join(' ');
            console.log("transfer to red");
        }
        if (this.props.persons.length <= 2) {
            assignedClasses.push(Classes.red);
        }
        if (this.props.persons.length <= 1) {
            assignedClasses.push(Classes.bold);
        }
        return (
            <Aux>
                <h1> Hello React </h1>
                <p className={assignedClasses.join(' ')}>This page working good</p>
                <button
                    className={btnClass}
                    onClick={this.props.clicked}
                >Switch Person</button>
            </Aux>
        )
    }
}
export default Cockpit;